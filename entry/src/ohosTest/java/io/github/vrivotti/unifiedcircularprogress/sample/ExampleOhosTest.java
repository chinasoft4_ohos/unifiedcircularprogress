package io.github.vrivotti.unifiedcircularprogress.sample;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExampleOhosTest {
    /**
     * 单元测试基础方法
     */
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("io.github.vrivotti.unifiedcircularprogress", actualBundleName);
    }
}