/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.vrivotti.unifiedcircularprogress.sample.slice;

import io.github.vrivotti.unifiedcircularprogress.ProgressBarExt;
import io.github.vrivotti.unifiedcircularprogress.sample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.AbsButton;
import ohos.agp.components.Slider;
import ohos.agp.components.Switch;

/**
 * 主页
 *
 * @author name
 * @since 2021-06-15
 */
public class MainAbilitySlice extends AbilitySlice {
    private Switch autoSwitch;
    private Slider slider;
    private ProgressBarExt progress;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initView();
    }

    private void initView() {
        autoSwitch = (Switch) findComponentById(ResourceTable.Id_autoSwitch);
        slider = (Slider) findComponentById(ResourceTable.Id_slider);
        progress = (ProgressBarExt) findComponentById(ResourceTable.Id_progress);
        autoSwitch.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean isAuto) {
                progress.setIndeterminate(isAuto);
            }
        });
        slider.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider sl, int progressValue, boolean isBool) {
            }

            @Override
            public void onTouchStart(Slider sl) {
            }

            @Override
            public void onTouchEnd(Slider sl) {
                progress.setProgress(slider.getProgress());
                autoSwitch.setChecked(false);
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
