# unifiedcircularprogress
## 项目介绍

 * 项目名称：unifiedcircularprogress
 * 所属系列：openharmony的第三方组件适配移植
 * 功能：圆形动态进度条
 * 项目移植状态：已完成
 * 调用差异：无
 * 开发版本：sdk6，DevEco Studio 2.2 Beta1
 * 基线版本：Relese 0.2.4

## 效果演示
<img src="gif/screen.gif"></image>

## 安装教程
在moudle级别下的build.gradle文件中添加依赖
```
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
```
在entry模块中的build.gradle文件中添加
```
dependencies {
    implementation ('com.gitee.chinasoft_ohos:unifiedCircularProgress:1.0.0')
    ····
}
```

在sdk6，DevEco Studio2.2 Beta1下项目可直接运行，

如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，

并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

## 使用说明
使用此组件
```
<io.github.vrivotti.unifiedcircularprogress.ProgressBarExt
        ohos:id="$+id:progress"
        ohos:height="50vp"
        ohos:width="50vp"/>
```

## 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

## 版本迭代
> * 1.0.0

## 版权和许可信息
```
MIT
```