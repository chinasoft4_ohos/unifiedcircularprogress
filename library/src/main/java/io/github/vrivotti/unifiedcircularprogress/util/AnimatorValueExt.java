/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.vrivotti.unifiedcircularprogress.util;

import ohos.agp.animation.AnimatorValue;

/**
 * 动画值计算
 *
 * @author name
 * @since 2021-06-15
 */
public class AnimatorValueExt extends AnimatorValue implements AnimatorValue.ValueUpdateListener {
    private AnimatorValueUtil animatorValueUtil;
    private ValueUpdateListener listener;
    private float currentValue;

    private AnimatorValueExt(float[] rate, float[] values) {
        animatorValueUtil = new AnimatorValueUtil();
        animatorValueUtil.ofFloat(rate, values);
        this.setValueUpdateListener(this);
    }

    /** ofFloat
     * @param rate 起始位置
     * @param values 起始值
     * @return AnimatorValueExt
     */
    public static AnimatorValueExt ofFloat(float[] rate, float[] values) {
        return new AnimatorValueExt(rate, values);
    }

    @Override
    public void onUpdate(AnimatorValue animatorValue, float value) {
        if (listener != null) {
            currentValue = animatorValueUtil.getValue(value);
            listener.onAnimationUpdate(this);
        }
    }

    /**
     * 获取动画进度
     *
     * @return float
     */
    public float getAnimatedValue() {
        return currentValue;
    }

    /** 设置动画监听
     * @param lis 动画监听
     */
    public void setValueUpdateListener(ValueUpdateListener lis) {
        this.listener = lis;
    }

    /** 数值更新监听
     * @since 2021-06-15
     */
    public interface ValueUpdateListener {
        /** onAnimationUpdate
         * @param animatorValue 动画值
         * @since 2021-06-15
         */
        void onAnimationUpdate(AnimatorValueExt animatorValue);
    }
}
