/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.github.vrivotti.unifiedcircularprogress.util;

/**
 * ex.
 * AnimatorValueUtil animatorValueUtil = new AnimatorValueUtil();
 * animatorValueUtil.ofFloat(0.1f,-3f,2f)
 * public void onUpdate(AnimatorValue animatorValue, float v) {
 * float currentV = animatorValueUtil.getValue(v);
 * }
 *
 * @since 2021-04-06
 */
public class AnimatorValueUtil {
    static final int RATE_SIZE = 2;
    static final int NEGATIVE = -1;
    private int rateSize = RATE_SIZE;
    private int negaTive = NEGATIVE;
    private float[] values;
    private float[][] rate;

    /**
     * 如传一个参数，则getValue 返回值与传入的 V 值一致
     * 如传入连续两个相同参数，则会出现除0异常
     * 传参试例: 0.8f,-1.2f,3.5f,0,5
     *
     * @param tempRate 说明
     * @param tempValues 起始值
     */
    public void ofFloat(float[] tempRate, float[] tempValues) {
        this.values = tempValues;
        rate = new float[this.values.length - 1][rateSize];
        for (int ii = 0; ii < rate.length; ii++) { // 计算每个变化值的占比和达到该变化值系数
            float temp = tempRate[ii + 1];
            rate[ii][0] = temp;
            float tempDuration = this.values[ii + 1] - this.values[ii];
            rate[ii][1] = (this.values[ii] > this.values[ii + 1] ? negaTive : 1)
                    * Math.abs(tempDuration / (temp - tempRate[ii]));
        }
    }

    /**
     * 根据 属性动画中value的值，计算当前值
     *
     * @param value 属性动画中的value
     * @return 通过计算后的值
     */
    public float getValue(float value) {
        for (int ii = 0; ii < rate.length; ii++) {
            if (value <= rate[ii][0] || value > 1) {
                return values[ii] + (ii == 0 ? value : value - rate[ii - 1][0]) * rate[ii][1];
            }
        }
        return value;
    }
}
